﻿using System.Collections;
using DG.Tweening;
using UnityEngine;
using TMPro;

public class ScoreTextController : MonoBehaviour
{
    #region Fields

    [SerializeField] private TextMeshProUGUI _scoreTMP;
    private RectTransform _scoreRectTransform;
    private int _score;
    private int _topScore;
    public int Score => _score;

    #endregion

    #region Init

    private void Start()
    {
        _scoreTMP = GetComponent<TextMeshProUGUI>();
        _scoreRectTransform = GetComponent<RectTransform>();
        _topScore = PlayerPrefs.GetInt("HighScore", 0);
        _scoreTMP.text = $"TOP {_topScore}";
    }

    #endregion

    #region OnEnable/OnDisable

    private void OnEnable()
    {
        ScoreTracker.ScoreIsUp += ScoreChangeHandler;
        PlayerController.LevelIsEnded += LevelEndHandler;
    }

    private void OnDisable()
    {
        ScoreTracker.ScoreIsUp -= ScoreChangeHandler;
        PlayerController.LevelIsEnded -= LevelEndHandler;
    }

    #endregion

    #region Handlers

    private void ScoreChangeHandler()
    {
        _score++;
        _scoreTMP.text = $"{_score}";

        StartCoroutine(ScoreUpAnimationRoutine());
    }

    private void LevelEndHandler()
    {
        ScoreTracker.ScoreIsUp -= ScoreChangeHandler;
        _scoreTMP.text = $"TOP {_topScore}";
        StartCoroutine(LevelEndAnimationRoutine());
    }

    #endregion

    #region Routines

    IEnumerator ScoreUpAnimationRoutine()
    {
        _scoreRectTransform.DOScale(3, 0f);
        _scoreRectTransform.DOScale(1, 1f);
        yield return null;
    }

    IEnumerator LevelEndAnimationRoutine()
    {
        _scoreRectTransform.DOAnchorPosX(-400, 0.5f);
        yield return new WaitForSeconds(2f);
        _scoreRectTransform.DOAnchorPosX(300, 1f).SetEase(Ease.OutBounce);
        PlayerController.LevelIsEnded -= LevelEndHandler;
    }

    #endregion
}
