﻿using DG.Tweening;
using UnityEngine;

public class TitleController : MonoBehaviour
{
    #region Fields

    private RectTransform _titleRectTransform;
    [SerializeField] private GameObject _reloadButton;

    #endregion
    
    #region Init

    private void Start()
    {
        _titleRectTransform = GetComponent<RectTransform>();
    }
    
    #endregion
    
    #region OnEnable/OnDisable

    private void OnEnable()
    {
        PlayerController.LevelIsEnded += LevelEndHandler;
        PlayerController.IsMoved += LevelStartHandler;
    }

    private void OnDisable()
    {
        PlayerController.LevelIsEnded -= LevelEndHandler;
        PlayerController.IsMoved -= LevelStartHandler;
    }

    #endregion
    
    #region Handlers

    private void LevelEndHandler()
    {
        _titleRectTransform.DOAnchorPos(new Vector2(0,300), 1f).SetEase(Ease.OutBounce).SetDelay(0.5f).From(new Vector2(-1000, 300)).OnComplete(() =>
        {
            _reloadButton.SetActive(true);
        });
        PlayerController.LevelIsEnded -= LevelEndHandler;

    }

    private void LevelStartHandler()
    {
        _titleRectTransform.DOAnchorPos(new Vector2(1000, 300), 1f).SetEase(Ease.InBounce);
        PlayerController.IsMoved -= LevelStartHandler;
    }

    #endregion
}
