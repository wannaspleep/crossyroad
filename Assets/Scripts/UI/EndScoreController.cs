﻿using DG.Tweening;
using TMPro;
using UnityEngine;

public class EndScoreController : MonoBehaviour
{
    #region Fields
    
    private RectTransform _rectTransform;
    [SerializeField] private ScoreTextController _scoreTextScript;
    [SerializeField] private TextMeshProUGUI _endScoreTMP;

    #endregion
    
    #region OnEnable/OnDisable

    private void OnEnable()
    {
        PlayerController.LevelIsEnded += LevelEndHandler;
    }

    private void OnDisable()
    {
        PlayerController.LevelIsEnded -= LevelEndHandler;
    }

    #endregion

    #region LevelEndHandler

    private void LevelEndHandler()
    {
        _endScoreTMP.text = $"YOUR SCORE IS {_scoreTextScript.Score}";
        GetComponent<RectTransform>().DOAnchorPosX(0, 0.8f);
        PlayerController.LevelIsEnded -= LevelEndHandler;
    }

    #endregion
}
