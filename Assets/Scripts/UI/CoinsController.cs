﻿using TMPro;
using UnityEngine;

public class CoinsController : MonoBehaviour
{
    #region Fields
    
    private TextMeshProUGUI _coinsTMP;
    private int _coins;

    #endregion
    
    #region Init

    private void Start()
    {
        _coinsTMP = GetComponent<TextMeshProUGUI>();
        _coins = PlayerPrefs.GetInt("Coins", 0);
        _coinsTMP.text = $"${_coins}";
    }
    #endregion
    
    #region OnEnable/OnDisable

    private void OnEnable()
    {
        PlayerController.LevelIsEnded += LevelEndHandler;
        CollectableController.CoinIsTaken += OnCoinTakeHandler;
    }

    private void OnDisable()
    {
        PlayerController.LevelIsEnded -= LevelEndHandler;
        CollectableController.CoinIsTaken -= OnCoinTakeHandler;
    }
    
    #endregion
    
    #region Handlers

    private void OnCoinTakeHandler()
    {
        _coins++;
        _coinsTMP.text = $"${_coins}";
    }

    private void LevelEndHandler()
    {
        PlayerPrefs.SetInt("Coins", _coins);
    }
    #endregion
}
