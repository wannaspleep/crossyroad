﻿using System;
using System.Collections;
using DG.Tweening;
using UnityEngine;
using MovementDirection = SwipeDetection.Movement;

public class PlayerController : MonoBehaviour
{
    #region Fields

    [SerializeField] private GameObject _heavenParticles;
    [SerializeField] private GameObject _waterParticles;
    

    private MovementDirection _direction;
    private SwipeDetection _swipeDetectionScript;
    private int _obstaclesMask;
    private Rigidbody _playerRigidbody;

    private Vector3 _newPosition;
    private bool _moveIsAllowed = true;

    #endregion
    
    #region Events

    public static event Action IsMoved;
    public static event Action LevelIsEnded;
    public static event Action MoveIsNotAllowed ;
    
    #endregion
    
    #region OnEnable/OnDisable

    private void OnEnable()
    {
        _swipeDetectionScript.MovementDetected += MovementHandler;
    }

    private void OnDisable()
    {
        _swipeDetectionScript.MovementDetected -= MovementHandler;
    }

    #endregion

    #region Init

    private void Awake()
    {
        _swipeDetectionScript = SwipeDetection.Instance;
    }
    
    void Start()
    {
        _obstaclesMask = LayerMask.GetMask("Obstacles");
        _playerRigidbody = GetComponent<Rigidbody>();
    }

    #endregion
    
    #region MovementHandler

    private void MovementHandler(MovementDirection direction)
    {
        if (!_moveIsAllowed) return;
        Vector3 currentPosition = transform.position;
        
        switch (direction)
        {
            case MovementDirection.Up:
                _newPosition = new Vector3(currentPosition.x, currentPosition.y, Mathf.Round(currentPosition.z + 1));
                Move(_newPosition);
                break;
            case MovementDirection.Down:
                _newPosition = new Vector3(currentPosition.x, currentPosition.y, Mathf.Round(currentPosition.z - 1));
                Move(_newPosition);
                break;
            case MovementDirection.Right:
                _newPosition = new Vector3(Mathf.Round(currentPosition.x + 1), currentPosition.y, currentPosition.z);
                Move(_newPosition);
                break;
            case MovementDirection.Left:
                _newPosition = new Vector3(Mathf.Round(currentPosition.x - 1), currentPosition.y, currentPosition.z);
                Move(_newPosition);
                break;
        }
    }

    private void Move(Vector3 direction)
    {
        transform.DOLookAt(direction, 0.1f).OnComplete(() =>
        {
            Debug.DrawLine(transform.position, transform.forward, Color.red, 1f);
            if (!Physics.Raycast(transform.position, transform.forward,out var hit, 1f, _obstaclesMask))
            {
                transform.DOMove(direction, 0.5f).OnComplete(() => IsMoved?.Invoke());
                transform.DOMoveY(1, 0.2f).SetEase(Ease.Linear);
                AudioManager.AudioManager.m_instance.PlaySFX("Jump");

            }
            Debug.Log($"{hit.collider.tag}");

        });
    }
    #endregion

    #region OnCollision/OnTrigger

    private void OnCollisionEnter(Collision other)
    {
        transform.SetParent(other.collider.CompareTag("Log") ? other.transform : null);
    }
    

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Water"))
        {
            _moveIsAllowed = false;
            Instantiate(_waterParticles, transform.position, _waterParticles.transform.rotation);
            AudioManager.AudioManager.m_instance.PlaySFX("Splash");
            MoveIsNotAllowed?.Invoke();
            LevelIsEnded?.Invoke();
        }
        else if (other.CompareTag("Deadly"))
        {
            AudioManager.AudioManager.m_instance.PlaySFX("Bump");
            _moveIsAllowed = false;
            Die();
            MoveIsNotAllowed?.Invoke();
            StartCoroutine(BackToHeavenRoutine());
        }
        else if (other.CompareTag("Border"))
        {
            _moveIsAllowed = false;
            MoveIsNotAllowed?.Invoke();
            StartCoroutine(BackToHeavenRoutine());
        }
    }

    #endregion

    private void Die()
    {
        transform.DOScaleY(0.01f, 0.5f);
        transform.DOScaleX(0.07f, 0.5f).SetEase(Ease.InOutQuint);
        transform.DOScaleZ(0.07f, 0.5f).SetEase(Ease.InOutQuint);
    }
    
    #region Routines

    IEnumerator BackToHeavenRoutine()
    {
        yield return new WaitForSeconds(1f);
        _playerRigidbody.useGravity = false;
        Instantiate(_heavenParticles, transform.position, _heavenParticles.transform.rotation);
        AudioManager.AudioManager.m_instance.PlaySFX("Choir");
        transform.DOMoveY(10, 3).SetEase(Ease.InExpo);
        yield return new WaitForSeconds(3);
        LevelIsEnded?.Invoke();
    }
    #endregion
}
