﻿using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;
public class ObstacleSpawner : MonoBehaviour
{
    #region Fields
    private int _maxObstacles = 4;
    private int _spawnBound = 6;
    private readonly string[] _obstacleKeys = {"Tree", "Rock", "Bush"};
    private readonly string _coinKey = "Coin";
    private Vector3 _spawnPosition;
    private ObjectPoolManager _pool;

    private Vector3[] _spawnPositions;
    #endregion
    
    #region Init

    private void Awake()
    {
        _pool = ObjectPoolManager.Instance;
    }
    
    private void Start()
    {
        SpawnObstacles();
    }
    
    #endregion
    
    #region OnEnable/OnDisable

   

    #endregion
    private void SpawnObstacles()
    {
        _spawnPosition = transform.position;
        int obstaclesCount = Random.Range(2, _maxObstacles + 1);
        _spawnPositions = new Vector3[obstaclesCount];
        for (int i = 0; i < obstaclesCount; i++)
        {
            do
            {
                _spawnPosition.x = Random.Range(-_spawnBound, _spawnBound);
                
            } while (CheckPreviousPositions());

            _spawnPositions[i] = _spawnPosition;
            var key = Random.Range(0, 10) != 0 ? _obstacleKeys[Random.Range(0, _obstacleKeys.Length)] : _coinKey;
            var obstacle = _pool.GetObject(key);
            obstacle.transform.SetParent(transform);
            obstacle.transform.position = _spawnPosition;
            obstacle.SetActive(true);
        }
    }

    private bool CheckPreviousPositions()
    {
        return _spawnPositions.Any(position => _spawnPosition == position);
    }
}
