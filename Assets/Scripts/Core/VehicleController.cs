﻿using System;
using System.Collections;
using DG.Tweening;
using UnityEngine;
using Random = UnityEngine.Random;

public class VehicleController : MonoBehaviour
{
    #region Fields

    [SerializeField] private float _minRideDuration;
    [SerializeField] private float _maxRideDuration;

    public Vector3 StartPoint { get; set; }

    public Vector3 EndPoint { get; set; }

    private GameObject _vehicle;

    private Tweener _moveTween;
    private Tweener _rotateTween;
    
    #endregion
    
    #region OnEnable/OnDisable

    private void OnEnable()
    {
        _rotateTween?.Restart();
        _moveTween?.OnComplete(OnCompleteTween).Restart();
    }

    #endregion
    
    #region Init

    private void Start()
    {
        Debug.Log($"I'm called: {gameObject.GetInstanceID()}");
        StartMove();
    }

    #endregion

    #region MoveCar Methods

    private void StartMove()
    {
        transform.position = StartPoint;
        transform.DOLookAt(EndPoint, 0f);
        StartCoroutine(Move());
    }

    IEnumerator Move()
    {
        var duration = Random.Range(_minRideDuration, _maxRideDuration);
        transform.DOMove(EndPoint, duration);
        yield return new WaitForSeconds(duration);
        OnCompleteTween();
    }
    private void OnCompleteTween()
    {
        transform.position = StartPoint;
        gameObject.SetActive(false);
    }
    #endregion
    
}
