﻿using System;
using System.Collections;
using DG.Tweening;
using UnityEngine;
using Direction = SwipeDetection.Movement;
public class CameraFollow : MonoBehaviour
{
    #region Fields

    [SerializeField] private Transform _target;

    private Vector3 _offset;
    private Tweener _tween;
    private Vector3 _targetLastPosition;
    private bool _isFollowAllowed = true;
    
    #endregion

    #region Init

    private void Start()
    {
        _offset = transform.position - _target.position;
        _tween = transform.DOMove(_target.position + _offset, 1).SetAutoKill(false);

        _targetLastPosition = _target.position + _offset;
    }

    #endregion

    #region OnEnable/OnDisable

    private void OnEnable()
    {
        PlayerController.MoveIsNotAllowed += EndFollowHandler;
    }

    private void OnDisable()
    {
        PlayerController.MoveIsNotAllowed -= EndFollowHandler;
    }

    #endregion

    private void Update()
    {
        if (!_isFollowAllowed) return;
        MovementHandler();
    }

    #region Handler

    private void MovementHandler()
    {
        if (_targetLastPosition == _target.position + _offset) return;
        _tween.ChangeEndValue(_target.position + _offset, true).Restart();

        _targetLastPosition = _target.position + _offset;
    }

    private void EndFollowHandler()
    {
        StartCoroutine(EndFollowRoutine());
    }
    
    #endregion

    #region Routines

    IEnumerator EndFollowRoutine()
    {
        yield return new WaitForSeconds(0.3f);
        _isFollowAllowed = false;
        PlayerController.LevelIsEnded -= EndFollowHandler;
    }

    #endregion
}
