﻿using System.Collections;
using DG.Tweening;
using UnityEngine;

public class SpawnVehicle : MonoBehaviour
{
    #region MoveVehicle

    private IEnumerator MoveCarRoutine(GameObject car)
    {
        car.transform.position = _startPoint;
        car.transform.DOLookAt(_endPoint, 0f);
        car.SetActive(true);
        car.transform.DOMove(_endPoint, _moveDuration).SetEase(Ease.Linear);
        yield return new WaitForSeconds(_moveDuration);
        car.SetActive(false);
        car.transform.parent = null;
    }

    #endregion

    #region Fields

    [SerializeField] private Transform[] _walkPoints;
    [SerializeField] private float _minSpawnDelay;
    [SerializeField] private float _maxSpawnDelay;
    [SerializeField] private float _minMoveDuration;
    [SerializeField] private float _maxMoveDuration;
    [SerializeField] private string[] _vehicleKeys = {"Vehicle1", "Vehicle2", "Vehicle3", "Vehicle4"};
    private Vector3 _startPoint;
    private Vector3 _endPoint;
    private ObjectPoolManager _pool;
    private float _spawnDelay;
    private float _moveDuration;

    #endregion

    #region Init

    private void Awake()
    {
        _pool = ObjectPoolManager.Instance;
    }
    private void Start()
    {
        SetDirection();
        _spawnDelay = Random.Range(_minSpawnDelay, _maxSpawnDelay);
        _moveDuration = Random.Range(_minMoveDuration, _maxMoveDuration);
        StartCoroutine(SpawnVehicles());
    }

    #endregion

    #region SpawnInit

    private void SetDirection()
    {
        if (Random.Range(0, 2) == 1)
        {
            _startPoint = _walkPoints[1].position;
            _endPoint = _walkPoints[0].position;
        }
        else
        {
            _startPoint = _walkPoints[0].position;
            _endPoint = _walkPoints[1].position;
        }
    }

    private IEnumerator SpawnVehicles()
    {
        while (true)
        {
            var vehicle = _pool.GetObject(gameObject.CompareTag("Water")
                ? "Log"
                : _vehicleKeys[Random.Range(0, _vehicleKeys.Length)]);
            vehicle.transform.SetParent(transform);
            vehicle.transform.position = Vector3.zero;
            StartCoroutine(MoveCarRoutine(vehicle));
            yield return new WaitForSeconds(_spawnDelay);
        }
    }

    #endregion
}
