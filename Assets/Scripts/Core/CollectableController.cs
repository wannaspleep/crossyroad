﻿using System;
using System.Collections;
using DG.Tweening;
using UnityEngine;
using Random = UnityEngine.Random;

public class CollectableController : MonoBehaviour
{
    #region Fields

    private bool _isTaken;
    private Vector3 _startPosition;
    
    #endregion
    
    #region Init

    private void Start()
    {
        _startPosition = transform.position;
        StartCoroutine(AnimationRoutine());
    }

    #endregion
    
    #region Events

    public static event Action CoinIsTaken;
    
    #endregion

    #region OnTriggerEnter

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            _isTaken = true;
            CoinIsTaken?.Invoke();
            AudioManager.AudioManager.m_instance.PlaySFX("Coin");
            gameObject.transform.parent.gameObject.SetActive(false);
        }
    }

    #endregion
    
    #region Routines

    IEnumerator AnimationRoutine()
    {
        while (!_isTaken)
        {
            yield return new WaitForSeconds(Random.Range(2, 5));
            transform.DOMoveY(1, 0.2f).SetEase(Ease.OutCubic);
            yield return new WaitForSeconds(0.2f);
            transform.DOMoveY(_startPosition.y, 0.5f).SetEase(Ease.OutBounce);
        }
    }
    #endregion
}
