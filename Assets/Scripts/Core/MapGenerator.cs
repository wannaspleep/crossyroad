﻿using System.Collections.Generic;
using UnityEngine;
using Direction = SwipeDetection.Movement;
using Random = UnityEngine.Random;


public class MapGenerator : MonoBehaviour
{
    #region Fields

    private List<GameObject> _activeTiles;
    private ObjectPoolManager _pool;
    private Vector3 _currentPosition = new Vector3(0, 0, -13f);
    [SerializeField] private int _maxLength;
    private int _currentLength;
    private MapTileData _currentTileData;
    private MapTileData _previousTileData;
    [SerializeField] private MapTileData[] _tileDates;
    private bool _flag;
    private bool _isTwoLaneFlag;

    #endregion

    #region Init

    private void Awake()
    {
        _pool = ObjectPoolManager.Instance;
    }

    private void Start()
    {
        _activeTiles = new List<GameObject>();
        InitMap();
    }

    private void InitMap()
    {
        for (int i = 0; i < 10; i++)
        {
            GameObject tile = _pool.GetObject("TGrass");
            tile.transform.position = _currentPosition;
            tile.SetActive(true);
            _activeTiles.Add(tile);
            _currentPosition.z += 2;
            _currentLength += 2;
        }
        _previousTileData = _activeTiles[0].GetComponent<TileDataHolder>().TileData;

        for (int i = 0; i < 5; i++)
        {
            SpawnTile();
        }
    }

    #endregion

    #region OnEnable/OnDisable

    private void OnEnable()
    {
        ScoreTracker.ScoreIsUp += MoveHandler;
    }

    private void OnDisable()
    {
        ScoreTracker.ScoreIsUp -= MoveHandler;
    }

    #endregion

    #region MoveUpHandler

    private void MoveHandler()
    {
        SpawnTile();
    }


    private void SpawnTile()
    {
        if (_currentLength < _maxLength)
        {
            while (true)
            {
                _currentTileData = _tileDates[Random.Range(0, _tileDates.Length)];
                if (_currentTileData.Type != _previousTileData.Type) break;
            }


            var tileCount = Random.Range(_currentTileData.minInSequence, _currentTileData.maxInSequence);

            for (int i = 0; i < tileCount; i++)
            {
                GameObject tile = _pool.GetObject(_currentTileData.Key);
                tile.transform.position = _currentPosition;
                _activeTiles.Add(tile);
                _currentLength += _currentTileData.step;
                _currentPosition.z += _currentTileData.step;
                tile.SetActive(true);
            }

            _previousTileData = _currentTileData;
        }

        MapTileData data = _activeTiles[0].GetComponent<TileDataHolder>().TileData;
        switch (data.step)
        {
            case 2 when _flag:
                _activeTiles[0].SetActive(false);
                _activeTiles.RemoveAt(0);
                _currentLength -= 2;
                _flag = false;
                break;
            case 2:
                _flag = true;
                break;
            case 1:
                _activeTiles[0].SetActive(false);
                _activeTiles.RemoveAt(0);
                _currentLength -= 1;
                break;
        }
    }

    #endregion
}
