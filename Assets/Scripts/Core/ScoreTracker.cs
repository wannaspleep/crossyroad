﻿using System;
using UnityEngine;
using Direction = SwipeDetection.Movement;

public class ScoreTracker : MonoBehaviour
{
    #region Fields
    private SwipeDetection _swipeDetectionScript;
    [SerializeField] private Transform _player;
    private Vector3 _maxPosition;
    private int _maxScore;
    private int _score;

    #endregion
    
    #region Events

    public static event Action ScoreIsUp;
    #endregion

    #region OnEnable/OnDisable

    private void OnEnable()
    {
        PlayerController.IsMoved += MovementHandler;
        PlayerController.LevelIsEnded += LevelEndHandler;
    }

    private void OnDisable()
    {
        PlayerController.IsMoved -= MovementHandler;
        PlayerController.LevelIsEnded -= LevelEndHandler;
    }

    #endregion

    #region Init

    private void Start()
    {
        _maxPosition = transform.position;
        _maxScore = PlayerPrefs.GetInt("HighScore", 0);
    }

    #endregion

    #region Handler

    private void MovementHandler()
    {
        if (transform.position.z > _maxPosition.z)
        {
            _score++;
            Debug.Log("Score Up");
            ScoreIsUp?.Invoke();
            _maxPosition = transform.position;
        }
    }

    private void LevelEndHandler()
    {
        if (_score > _maxScore)
        {
            PlayerPrefs.SetInt("HighScore", _score);
        }
    }
    
    #endregion

}
