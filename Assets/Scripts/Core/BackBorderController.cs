﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class BackBorderController : MonoBehaviour
{
    #region Fields
    
    [Tooltip("The smaller the faster")]
    [SerializeField] private float _moveSpeed;

    private bool _isMoveAllowed;
    #endregion
    
    #region Init
    #endregion
    
    #region OnEnable/OnDisable

    private void OnEnable()
    {
        PlayerController.IsMoved += AllowMove;
    }

    private void OnDisable()
    {
        PlayerController.IsMoved -= AllowMove;
    }

    private void AllowMove()
    {
        _isMoveAllowed = true;
        StartCoroutine(MoveBorderRoutine());
    }

    #endregion
    
    #region Routines

    IEnumerator MoveBorderRoutine()
    {
        PlayerController.IsMoved -= AllowMove;
        
        while (_isMoveAllowed)
        {
            transform.DOMoveZ(transform.position.z + 1, _moveSpeed).SetEase(Ease.Linear);
            yield return new WaitForSeconds(_moveSpeed);
        }
    }

    #endregion
    
    #region OnTriggerEnter

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player")) _isMoveAllowed = false;
    }

    #endregion
}
