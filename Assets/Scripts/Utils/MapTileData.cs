﻿using UnityEngine;

[CreateAssetMenu(fileName = "Tile Data", menuName = "Tile Data")]
public class MapTileData : ScriptableObject
{
    public GameObject tilePrefab;
    [SerializeField] private int type;
    [SerializeField] private string key;
    public int Type => type;
    public string Key => key;
    public int minInSequence;
    public int maxInSequence;
    public int step;
}
