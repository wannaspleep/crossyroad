﻿using UnityEngine;

public class TileDataHolder : MonoBehaviour
{
    [SerializeField] private MapTileData _tileData;
    public MapTileData TileData => _tileData;
}
