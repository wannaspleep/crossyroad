﻿using System;
using UnityEngine;
using UnityEngine.InputSystem;

[DefaultExecutionOrder(-1)]
public class InputManager : MonoBehaviourSingleton<InputManager>
{
    #region Fields
    
    private PlayerControls _playerControls;
    
    #endregion

    #region Events
    //The first argument is position, the second is time
    public event Action<Vector2, float> TouchIsStarted;
    public event Action<Vector2, float> TouchIsEnded;
    
    #endregion

    #region Init

    private void Awake()
    {
        _playerControls = new PlayerControls();
    }

    private void Start()
    {
        _playerControls.Touch.PrimaryContact.started += StartTouchPrimary;
        _playerControls.Touch.PrimaryContact.canceled += EndTouchPrimary;
    }

    #endregion
   
    #region Methods
    private void EndTouchPrimary(InputAction.CallbackContext ctx)
    {
        Vector2 touchPosition = _playerControls.Touch.PrimaryTouch.ReadValue<Vector2>();
        TouchIsEnded?.Invoke(touchPosition, (float) ctx.time);
    }
    private void StartTouchPrimary(InputAction.CallbackContext ctx)
    {
        Vector2 touchPosition = _playerControls.Touch.PrimaryTouch.ReadValue<Vector2>();
        TouchIsStarted?.Invoke(touchPosition, (float) ctx.startTime);
    }
    #endregion

    #region OnEnable/OnDisable

    private void OnEnable()
    {
        _playerControls.Enable();
    }

    private void OnDisable()
    {
        _playerControls.Disable();
    }

    #endregion

}