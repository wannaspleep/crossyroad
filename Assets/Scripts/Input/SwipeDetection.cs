﻿using System;
using UnityEngine;

public class SwipeDetection : MonoBehaviourSingleton<SwipeDetection>
{
    #region Fields

    private InputManager _inputManager;
    [SerializeField] private float _minimumDistance;
    [SerializeField, Range(0f, 0.1f)] private float _tapTimeThreshold;
    [SerializeField, Range(0.1f, 1)] private float _swipeTimeThreshold;
    [SerializeField, Range(0, 1f)] private float _directionThreshold;

    private Vector2 _startPosition;
    private float _startTime;
    private Vector2 _endPosition;
    private float _endTime;

    #endregion

    #region Events

    public event Action<Movement> MovementDetected;

    #endregion

    #region Init

    private void Awake()
    {
        _inputManager = InputManager.Instance;
    }

    #endregion


    #region OnEnable/OnDisable

    private void OnEnable()
    {
        _inputManager.TouchIsStarted += SwipeStartHandler;
        _inputManager.TouchIsEnded += SwipeEndHandler;
    }

    private void OnDisable()
    {
        _inputManager.TouchIsStarted -= SwipeStartHandler;
        _inputManager.TouchIsEnded -= SwipeEndHandler;
    }

    #endregion

    private void SwipeStartHandler(Vector2 position, float startTime)
    {
        _startPosition = position;
        _startTime = startTime;
    }
    private void SwipeEndHandler(Vector2 position, float endTime)
    {
        _endPosition = position;
        _endTime = endTime;
        SwipeDetector(_endTime - _startTime);
    }


    private void SwipeDetector( float deltaTime)
    {
        float distance = Vector3.Distance(_startPosition, _endPosition);
        Debug.Log($"Distance {distance}");
        
        if (deltaTime > _tapTimeThreshold && distance > _minimumDistance)
        {
            Debug.Log("It's a swipe");
            Vector3 direction = _endPosition - _startPosition;
            var direction2D = new Vector2(direction.x, direction.y).normalized;
            SwipeDirection(direction2D);
        }
        else
        {
            Debug.Log("It's a tap");
            MovementDetected?.Invoke(Movement.Up);
        }
    }

    private void SwipeDirection(Vector2 direction)
    {
        if (Vector2.Dot(Vector2.up, direction) > _directionThreshold)
        {
            MovementDetected?.Invoke(Movement.Up);
        }
        else if (Vector2.Dot(Vector2.down, direction) > _directionThreshold)
        {
            MovementDetected?.Invoke(Movement.Down);

        }
        else if (Vector2.Dot(Vector2.right, direction) > _directionThreshold)
        {
            MovementDetected?.Invoke(Movement.Right);

        }
        else if (Vector2.Dot(Vector2.left, direction) > _directionThreshold)
        {
            MovementDetected?.Invoke(Movement.Left);

        }
    }
    
    public enum Movement
    {
        Up,
        Down,
        Left,
        Right
    }
}
