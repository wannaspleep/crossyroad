﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace AudioManager
{
    public class VolumeManager : MonoBehaviour
    {
        #region Fields

        private RectTransform _buttonRectTransform;
        private Image _buttonImage;
        [SerializeField] private Sprite _offSound;
        [SerializeField] private Sprite _onSound;
        private int _isSoundActive = 1;

        #endregion

        #region OnEnable/OnDisable

        private void OnEnable()
        {
            PlayerController.LevelIsEnded += LevelEndHandler;
        }

        private void OnDisable()
        {
            PlayerController.LevelIsEnded -= LevelEndHandler;
        }

        #endregion
        #region Init

        private void Start()
        {
            _buttonImage = transform.GetChild(0).GetComponent<Image>();
            _isSoundActive = PlayerPrefs.GetInt("Volume", 1);
            _buttonImage.sprite = _isSoundActive == 1 ? _onSound : _offSound;
            AudioListener.volume = _isSoundActive;
        }

        #endregion
        
        #region Volume

        public void ChangeVolume()
        {
            _isSoundActive = _isSoundActive == 1 ? 0 : 1;
            _buttonImage.sprite = _isSoundActive == 1 ? _onSound : _offSound;
            AudioListener.volume = _isSoundActive;
            PlayerPrefs.SetInt("Volume", _isSoundActive);
        }
        
        #endregion
        
        #region Handler

        private void LevelEndHandler()
        {
            GetComponent<RectTransform>().DOAnchorPosX(-144, 1f).SetEase(Ease.OutBounce);
        }
        #endregion
    }
    
}
