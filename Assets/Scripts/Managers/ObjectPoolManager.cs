﻿using System;
using System.Collections.Generic;
using UnityEngine;

[DefaultExecutionOrder(-1)]
public class ObjectPoolManager : MonoBehaviourSingleton<ObjectPoolManager>
{
    [SerializeField] private List<Pool> _pools;
    [SerializeField] private Transform _levelHolder;
    private Dictionary<string, List<GameObject>> _poolDictionary;

    [Serializable]
    private class Pool
    {
        public string _key;
        public GameObject _prefab;
        public int size;
    }


    private void Start()
    {
        _poolDictionary = new Dictionary<string, List<GameObject>>();

        foreach (var pool in _pools)
        {
            List<GameObject> newPool = new List<GameObject>(pool.size);

            for (var i = 0; i < pool.size; i++)
            {
                GameObject pooledObject = Instantiate(pool._prefab, Vector3.zero, pool._prefab.transform.rotation);
                pooledObject.transform.SetParent(_levelHolder);
                pooledObject.SetActive(false);
                newPool.Add(pooledObject);
            }

            _poolDictionary.Add(pool._key, newPool);
        }
    }

    public GameObject GetObject(string key)
    {
        if (!_poolDictionary.ContainsKey(key)) return null;

        for (int i = 0; i < _poolDictionary[key].Count; i++)
        {
            GameObject obj = _poolDictionary[key][i];
            if (!obj.activeSelf) return obj;
        }
        foreach (var pool in _pools)
        {
            if (pool._key.Equals(key))
            {
                var pooledObject = Instantiate(pool._prefab, Vector3.zero, pool._prefab.transform.rotation);
                pooledObject.transform.SetParent(_levelHolder);
                pooledObject.SetActive(false);
                _poolDictionary[key].Add(pooledObject);
                return pooledObject;
            }
        }
        return null;
    }
}
